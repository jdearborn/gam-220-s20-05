#include <iostream>
using namespace std;

void PrintThis(int value)
{
	//int someOtherValue = 5;

	cout << value << endl;
}

int main(int argc, char* argv[])
{
	PrintThis(67);

	// Set up an array that holds 10 different (arbitrary) grade values from 0-100.
	int gradeArray[10] = {70, 50, 30, 20, 10, 40, 80, 90, 100, 60};

	for (int i = 0; i < 10; ++i)
	{
		gradeArray[i] = rand() % 101;
	}

	gradeArray[0] = 70;
	gradeArray[1] = 50;
	gradeArray[2] = 30;
	gradeArray[3] = 20;
	gradeArray[4] = 10;
	gradeArray[5] = 40;
	gradeArray[6] = 80;
	gradeArray[7] = 91;
	gradeArray[8] = 100;
	gradeArray[9] = 60;

	/*int j = 0;
	cout << ++j << endl;  // 1
	cout << j << endl;  // 1

	j = 0;
	cout << j++ << endl;  // 0
	cout << j << endl;  // 1
	*/

	// Print out all of the grades
	for (int i = 0; i < 10; ++i)
	{
		cout << gradeArray[i] << endl;
	}


	// Print only the passing grades
	for (int i = 0; i < 10; ++i)
	{
		if (gradeArray[i] > 60)
		{
			cout << gradeArray[i] << endl;
		}
	}


	// "Remove" the failing grades from the array
	for (int i = 0; i < 10; ++i)
	{
		if (gradeArray[i] < 60)
		{
			gradeArray[i] = -1;
		}
	}


	// Calculate and print the average of all the remaining grades
	int sum = 0;
	int count = 0;
	float average;
	for (int i = 0; i < 10; ++i)
	{
		if (gradeArray[i] >= 0)
		{
			sum += gradeArray[i];
			count++;
			//++count;
			//count = count + 1;
			//count += 1;
		}
		
	}
	average = sum / (float)count;
	cout << "Average: " << average << endl;



	return 0;
}
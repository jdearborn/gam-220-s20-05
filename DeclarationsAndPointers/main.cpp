#include <iostream>  // Angle brackets because this is a standard include file
#include "PrintJon.h"  // double-quotes because this is part of my project
using namespace std;


int main(int argc, char* argv[])
{
	PrintJon();


	int bob = 5;

	// A pointer is a memory address
	// This is a pointer
	int* bobsAddress = &bob;  // & as the "address-of" operator

	cout << "Bob: " << bob << endl;

	// Derefencing a pointer using the * operator
	cout << "Bob via memory address: " << *bobsAddress << endl;

	int definitelyNotBob = 17;

	bobsAddress = &definitelyNotBob;
	cout << "This is not Bob's value: " << *bobsAddress << endl;

	// Let's request some memory from the OS
	int* otherPointer = new int;
	*otherPointer = 10;

	// All done with that memory
	delete otherPointer;

	// Always match 'new' with a 'delete'

	// Bad!  This would be a "memory leak".  This memory would be unavailable for use until the program ends.
	/*for (int i = 0; i < 1000000; ++i)
	{
		int* z = new int;
	}*/


	return 0;
}
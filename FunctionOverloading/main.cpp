#include <iostream>
using namespace std;

void PrintNumber(float num)
{
	cout << num << endl;
}

void PrintNumber(double num)
{
	cout << "This is a double: " << num << endl;
}

int AddNumbers(int a, int b, int c, int d = 0, int e = 0)
{
	return a + b + c + d;
}

int main(int argc, char* argv[])
{
	PrintNumber(3.0f);

	PrintNumber(3.0);

	//PrintNumber(3);

	return 0;
}
#include <iostream>
using namespace std;

class ClampedInt
{
public:
	int value;
	int minValue;
	int maxValue;

	ClampedInt()
		: value(0), minValue(0), maxValue(10)
	{}
	ClampedInt(int newValue, int newMinValue, int newMaxValue)
		: value(newValue), minValue(newMinValue), maxValue(newMaxValue)
	{}

	ClampedInt operator+(int b)
	{
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;

		i.value = i.value + b;
		if (i.value < i.minValue)
			i.value = i.minValue;
		if (i.value > i.maxValue)
			i.value = i.maxValue;

		return i;
	}

	ClampedInt operator-()
	{
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;

		i.value = -i.value;
		if (i.value < i.minValue)
			i.value = i.minValue;
		if (i.value > i.maxValue)
			i.value = i.maxValue;

		return i;
	}

	ClampedInt operator-(int b)
	{
		ClampedInt i;
		i.value = value;
		i.minValue = minValue;
		i.maxValue = maxValue;

		i.value = i.value - b;
		if (i.value < i.minValue)
			i.value = i.minValue;
		if (i.value > i.maxValue)
			i.value = i.maxValue;

		return i;
	}
};

int main(int argc, char* argv[])
{
	ClampedInt i(4, -14, 12);

	i = i + 16;
	cout << i.value << endl;

	i = -i;
	cout << i.value << endl;

	return 0;
}
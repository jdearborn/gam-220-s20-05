#include <iostream>
#include "SDL.h"
using namespace std;

class Image
{
public:
	Image(SDL_Renderer* newRenderer, int newWidth, int newHeight)
	{
		width = newWidth;
		height = newHeight;
		renderer = newRenderer;
		texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_TARGET, width, height);
		if (texture == nullptr)
		{
			cout << "Error creating a texture! (" << SDL_GetError() << ")" << endl;
		}
	}

	~Image()
	{
		SDL_DestroyTexture(texture);
	}

	void Free()
	{
		SDL_DestroyTexture(texture);
		texture = nullptr;
	}

	int GetWidth() const
	{
		return width;
	}
	
	int GetHeight() const
	{
		return height;
	}

	void SetAsTarget()
	{
		SDL_SetRenderTarget(renderer, texture);
	}

	void UnsetAsTarget()
	{
		SDL_SetRenderTarget(renderer, nullptr);
	}

	// Warning: Very unoptimized/slow to render individual pixels
	void SetPixel(int x, int y, Uint8 red, Uint8 green, Uint8 blue)
	{
		SDL_SetRenderDrawColor(renderer, red, green, blue, 0xFF);
		SDL_RenderDrawPoint(renderer, x, y);
	}

	void DrawToScreen(float x, float y)
	{
		SDL_Rect destinationArea = { x, y, width, height };
		SDL_RenderCopy(renderer, texture, nullptr, &destinationArea);
	}

private:
	int width, height;
	SDL_Renderer* renderer;
	SDL_Texture* texture;
};



void UpdateImage(Image& image)
{
	image.SetAsTarget();

	for (int row = 0; row < image.GetHeight(); ++row)
	{
		for (int column = 0; column < image.GetWidth(); ++column)
		{
			Uint8 red = (float(row) / image.GetHeight()) * 255;
			Uint8 green = 255;
			Uint8 blue = 255;

			image.SetPixel(column, row, red, green, blue);
		}
	}

	image.UnsetAsTarget();
}


int main(int argc, char* argv[])
{
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		cout << "SDL failed to initialize! (" << SDL_GetError() << ")" << endl;
		return 1;
	}

	SDL_Window* window = SDL_CreateWindow("My Image Generator",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		500, 500, SDL_WINDOW_OPENGL);
	
	if (window == nullptr)
	{
		cout << "Error creating a window! (" << SDL_GetError() << ")" << endl;
		return 2;
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr)
	{
		cout << "Error creating a renderer! (" << SDL_GetError() << ")" << endl;
		return 3;
	}


	Image image(renderer, 500, 500);

	bool imageNeedsUpdate = true;
	const Uint8* keystates = SDL_GetKeyboardState(nullptr);
	int x = 0, y = 0;

	SDL_Event event;
	bool done = false;
	while (!done)
	{
		// Process input events and keys pressed this frame
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				done = true;
			if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_ESCAPE)
					done = true;
			}
		}

		// Check for keys held down
		if (keystates[SDL_SCANCODE_UP])
			y--;
		else if (keystates[SDL_SCANCODE_DOWN])
			y++;

		if (keystates[SDL_SCANCODE_LEFT])
			x--;
		if (keystates[SDL_SCANCODE_RIGHT])
			x++;



		SDL_SetRenderDrawColor(renderer, 100, 0, 0, 255);
		SDL_RenderClear(renderer);

		if (imageNeedsUpdate)
		{
			UpdateImage(image);
			imageNeedsUpdate = false;
		}

		image.DrawToScreen(x, y);

		SDL_RenderPresent(renderer);


		// Give some time back to the OS
		SDL_Delay(1);
	}


	image.Free();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
#include <iostream>
#include <vector>
using namespace std;

int main(int argc, char* argv[])
{
	vector<int> myVector;

	myVector.push_back(5);
	myVector.push_back(7);
	myVector.push_back(0);

	cout << "Vector contents: " << endl;
	for (int i = 0; i < myVector.size(); ++i)
	{
		cout << myVector[i] << " ";
	}
	cout << endl;

	cout << "Traversal using an iterator:" << endl;
	for (vector<int>::iterator iter = myVector.begin(); iter != myVector.end(); ++iter)
	{
		cout << *iter << " ";
	}
	cout << endl;

	cin.get();
	return 0;
}
#include <iostream>
#include "Health.h"
//#include "Player.h"
#include "MyClass.h"
using namespace std;





int main(int argc, char* argv[])
{
	MyClass hgasdfkjls;

	/*int myNum;
	typedef double MySpecialType;
	MySpecialType myOtherNumber = 0.5;*/

	// C# has to do it: Health myHealth = new Health();

	// Allocating on the "stack":
	int myNum;  // 4 bytes allocated in the stack memory space
	Health myHealth;

	// Allocating memory from the "heap":
	int* numInRAM = new int();  // sizeof(int) == 4 (bytes)
	Health* yourHealth = new Health();  // 12 bytes?  What is sizeof(Health)?  4.

	cout << sizeof(Health) << endl;


	// Syntax for accessing members of an object
	// For stack-allocated objects:
	myHealth.Damage(4);
	myHealth.Heal(2);

	// For heap-allocated objects:
	// Have to "dereference" the pointer first
	(*yourHealth).Damage(7);  // This is nasty!
	yourHealth->Heal(3);  // Some "syntactic sugar" for doing both the dereference and the member access





	// All done with heap memory?  delete it.
	delete numInRAM;
	delete yourHealth;



	return 0;
}
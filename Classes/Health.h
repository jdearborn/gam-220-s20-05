#pragma once

#include <string>
//using namespace std;
//using std::string;


class Health
{
public:  // C++ uses regions of public/private instead of tagging every member

	std::string name;

	Health()
		: hitpoints(10), name("Jon")  // initializer list
	{
		//cout << "Name: \"" << name << "\"" << endl;

		//hitpoints = 10;
		//name = "Jon";
	}

	int GetHitpoints()
	{
		return hitpoints;
	}

	void Heal(int amount)
	{
		hitpoints += amount;
	}

	void Damage(int amount)
	{
		hitpoints -= amount;
	}

private:
	int hitpoints;

};  // C++ requires a semi-colon at the end of a class definition

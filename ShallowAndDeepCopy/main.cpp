#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	int a = 0;

	// Plain-Old Data (POD)
	a = 5;  // Direct copy of that value into the variable


	// Not POD
	int stackArray[25];
	int* b = new int[100];
	// Initialize using a loop
	for (int i = 0; i < 100; ++i)
	{
		b[i] = 0;
	}


	int* c = new int[50];
	// A quick way to initialize these 200 bytes to 0.
	memset(c, 0, 50 * sizeof(int));

	// Shallow copy: Copy just the pointer value (memory location)
	// Copy a new value into b
	//b = c;
	// This shallow copy is a bad idea!  We lose access to the original array pointed at by b.
	// That is a memory leak!


	// Instead, let's do a "deep copy".
	// The deep copy goes through the array and copies each element instead.
	delete[] b;
	b = new int[50];
	for (int i = 0; i < 50; ++i)
	{
		b[i] = c[i];
	}

	// memcpy(b, c, 50 * sizeof(int));  // Only if b and c arrays don't overlap



	delete[] b;


	cin.get();
	return 0;
}
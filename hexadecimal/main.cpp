#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// Hex vs decimal

	// Hex is base 16
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, a, b, c, d, e, f, 10

	// Decimal is base 10
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

	cout << "Here's a decimal value: " << 16 << endl;
	cout << "This is a hex literal: " << 0x10 << endl;


	// Hex memory addresses
	int* myInt = new int();
	cout << "Memory address: " << myInt << endl;
	delete myInt;
	

	// Hex colors
	unsigned int color = 0xffa6bb00;
	

	// Formatting output for hex
	cout << std::hex << 32 << ", " << 0x20 << endl;


	// Reverting formatting back to decimal
	cout << 15 << endl;
	cout << std::dec;
	cout << 15 << endl;

	// Power of twos in hex
	//0b00001  // 2^0
	//0b00010  // 2^1
	//0b00100  // 2^2

	/*0x0001  // 2^0
	0x0002  // 2^1
	0x0004  // 2^2
	0x0008  // 2^3
	0x0010  // 2^4
	0x0020  // 2^5
	0x0040
	0x0080
	0x0100*/

	

	// A hex editor view of your binary



	return 0;
}
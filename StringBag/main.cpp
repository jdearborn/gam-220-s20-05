#include <iostream>
#include "StringBag.h"
using namespace std;

int main(int argc, char* argv[])
{
	// Demonstrate that the StringBag class works properly.

	cout << "Default constructor" << endl;
	StringBag bag1;

	cout << "bag1 size (should be 0): " << bag1.Size() << endl;
	cout << "bag1 capacity (should be 10): " << bag1.GetCapacity() << endl;

	cout << "Single-argument constructor" << endl;
	StringBag bag2(27);

	cout << "bag2 size (should be 0): " << bag2.Size() << endl;
	cout << "bag2 capacity (should be 27): " << bag2.GetCapacity() << endl;

	cout << "Testing destructor" << endl;
	StringBag* bag3 = new StringBag();
	delete bag3;  // This automatically calls the destructor
	
	cout << "Adding to bag1" << endl;
	bag1.Add("Test1");

	for (int i = 0; i < bag1.Size(); ++i)
	{
		cout << "In bag1: " << bag1.At(i) << endl;
	}

	cout << "Adding to bag2" << endl;
	string stringArray[] = {"Array1", "Array2", "Array3"};
	bag2.Add(stringArray, 3);

	for (int i = 0; i < bag2.Size(); ++i)
	{
		cout << "In bag2: " << bag2.At(i) << endl;
	}


	cout << "Adding \"Array2\" to bag1" << endl;
	bag1.Add("Array2");
	cout << "Union of bag1 and bag2" << endl;
	bag1.Union(&bag2);

	for (int i = 0; i < bag1.Size(); ++i)
	{
		cout << "In bag1: " << bag1.At(i) << endl;
	}

	cout << "Adding bag2 into bag1" << endl;
	bag1.Add(&bag2);

	for (int i = 0; i < bag1.Size(); ++i)
	{
		cout << "In bag1: " << bag1.At(i) << endl;
	}

	cout << "Removing \"Array3\" from bag1" << endl;
	bag1.Remove("Array3");

	for (int i = 0; i < bag1.Size(); ++i)
	{
		cout << "In bag1: " << bag1.At(i) << endl;
	}

	if (bag1.Contains("Array3"))
		cout << "bag1 constains \"Array3\"" << endl;
	else
		cout << "bag1 does not contain \"Array3\"" << endl;

	if (bag1.Contains("BadString"))
		cout << "bag1 constains \"BadString\"" << endl;
	else
		cout << "bag1 does not contain \"BadString\"" << endl;

	cout << "Finding \"Array2\": " << bag1.Find("Array2") << endl;
	cout << "Finding \"NotInBag\" (should be -1): " << bag1.Find("NotInBag") << endl;

	cout << "\"Array3\" occurs " << bag1.CountOccurrences("Array3") << " times." << endl;
	cout << "\"NotInBag\" occurs " << bag1.CountOccurrences("NotInBag") << " times. (should be 0)" << endl;

	cin.get();
	return 0;
}
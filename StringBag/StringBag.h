#pragma once
#include <string>
using std::string;

class StringBag
{
public:


	StringBag()
	{
		// Set up an empty StringBag that can contain up to 10 strings.
		capacity = 10;
		contents = new string[capacity];
		size = 0;
	}

	StringBag(int initialCapacity)
	{
		// Set up an empty StringBag that can contain up to `initialCapacity` strings.
		capacity = initialCapacity;
		contents = new string[capacity];
		size = 0;
	}

	// Adds a string to the bag.  If there is no room left, does nothing.
	void Add(string newString)
	{
		if (size < capacity)
		{
			contents[size] = newString;
			size++;
		}
	}

private:

	string* contents;
	int size;
	int capacity;
};

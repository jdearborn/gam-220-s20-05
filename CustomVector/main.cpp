#include <iostream>
#include "Vector.h"
using namespace std;

int main(int argc, char* argv[])
{
	Vector<int> v;

	for (int i = 0; i < 20; ++i)
	{
		v.PushBack(i);
	}

	for (int i = 0; i < v.Size(); ++i)
	{
		cout << v.At(i) << " ";
	}
	cout << endl;



	Vector<string> stringVector;

	stringVector.PushBack("Hello!");

	cout << stringVector.At(0) << endl;

	return 0;
}
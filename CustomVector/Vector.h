#pragma once
#include <string>
using std::string;

template<typename T>
class Vector
{
public:


	Vector();

	Vector(int initialCapacity);

	// Adds an integer to the vector.
	void PushBack(T value);

	void Reserve(int newCapacity);

	void Resize(int newSize);

	int Size();

	T& At(int index);

	T& operator[](int index)
	{
		return contents[index];
	}

private:

	T* contents;
	int size;
	int capacity;
};


#include "Vector.inl"
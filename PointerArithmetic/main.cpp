#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	// Pointer arithmetic: Doing math with/on memory addresses (pointers).

	int* myArray = new int[10];
	for (int i = 0; i < 10; ++i)
	{
		myArray[i] = i * 3;
	}

	int* a = myArray;

	cout << "Starting memory address for `a`: " << a << endl;
	cout << "Starting value for `a`: " << *a << endl;

	a++;  // ++ operator: Adds 1 and stores the result back in `a`.

	cout << "Next memory address for `a`: " << a << endl;
	cout << "Next value for `a`: " << *a << endl;

	a += 8;

	cout << "Last memory address for `a`: " << a << endl;
	cout << "Last value for `a`: " << *a << endl;

	int* endAddress = a + 1;


	cout << "Printing the array using pointer arithmetic to iterate through each value..." << endl;
	for (int* p = myArray; p != endAddress; ++p)
	{
		cout << *p << " ";
	}
	cout << endl;

	cout << "Printing a subarray (position 3 to the end) using pointer arithmetic..." << endl;
	for (int* p = myArray + 3; p != endAddress; ++p)
	{
		cout << *p << " ";
	}
	cout << endl;


	cout << endl;


	// Abusing pointer arithmetic for fun!
	int x = 0;

	while (true)
	{
		char c = cin.get();
		if (c == 'a')
		{
			--x;
			while (*("Hello" + x) != 0)  // '\0' == 0   -> This is the null terminator byte
			{
				--x;
			}
		}
		else if (c == 'd')
			x++;
		cout << "Hello" + x << endl;
		cout << (int)("Hello" + x) << endl;
	}

	cin.get();
	return 0;
}
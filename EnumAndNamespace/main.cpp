#include <iostream>
using namespace std;

/*
enum (enumeration)
Scoped enum
switch
namespace
*/

enum class SuccessEnum
{
	FAIL,
	SUCCESS,
	UNKNOWN
};

SuccessEnum DoSomeWork()
{
	cout << "Doing work..." << endl;
	return SuccessEnum::SUCCESS;
}

namespace EnemyStuff
{
	enum EnemyType
	{
		GOBLIN,
		ORC,
		TROLL,
		DEMON
	};

	class Enemy
	{
	public:
		int hitpoints;
		EnemyType type;
	};
}

class Goblin : public EnemyStuff::Enemy
{
public:
	Goblin()
	{
		hitpoints = 5;
		type = EnemyStuff::GOBLIN;  // Tip: If you ever do this, there's probably a better way!
	}
};


int main(int argc, char* argv[])
{
	// Enums can be converted to and from integers.
	/*SuccessEnum myValue;
	myValue = (SuccessEnum)2;*/

	if (DoSomeWork() == SuccessEnum::SUCCESS)
	{
		cout << "That was successful!" << endl;
	}


	SuccessEnum resultOfWork = DoSomeWork();
	if (resultOfWork == SuccessEnum::SUCCESS)
	{
		cout << "That was successful!" << endl;
	}
	else if (resultOfWork == SuccessEnum::FAIL)
	{
		cout << "That was not successful..." << endl;
	}
	else if (resultOfWork == SuccessEnum::UNKNOWN)
	{
		cout << "I'm not sure what happened..." << endl;
	}

	switch (DoSomeWork())
	{
	case SuccessEnum::SUCCESS:
		cout << "That was successful!" << endl;
		break;
	case SuccessEnum::FAIL:
		cout << "That was not successful..." << endl;
		break;
	case SuccessEnum::UNKNOWN:
		cout << "I'm not sure what happened..." << endl;
		break;
	}

	int myNum = 5;
	switch (myNum)
	{
	case 2:
		cout << "2" << endl;
		break;
	case 5:
		cout << "5" << endl;
		//break;  // break is "optional": without one, you fall through to the next case's statements.
	case 0:
		cout << "0" << endl;
		break;
	default:
		cout << "It is none of those values!" << endl;
		break;
	}



	// NEVER USE GOTO!!!
	while (true) // : OuterLoop   <-- This doesn't exist yet.
	{
		for (int i = 0; i < 7; ++i)
		{
			if (i == 3)
			{
				// No such thing as "multi-level break" (yet...)
				// break 2;

				// No such thing as named loops (yet...)
				// break OuterLoop;
				goto OuterLoop;
			}
		}
	}
	OuterLoop:

	cin.get();
	return 0;
}
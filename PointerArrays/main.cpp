#include <iostream>
using namespace std;

void PrintArray(int* array, int size)
{
	for (int i = 0; i < size; ++i)
	{
		cout << array[i] << " ";
	}
	cout << endl;
}



int main(int argc, char* argv[])
{
	int stackArray[6];
	for (int i = 0; i < 6; ++i)
	{
		stackArray[i] = i + 1;
	}

	cout << "Stack array: " << endl;
	PrintArray(stackArray, 6);


	int* heapArray;
	heapArray = new int[10];

	for (int i = 0; i < 10; ++i)
	{
		heapArray[i] = i * 2;
	}

	cout << "Heap array: " << endl;
	PrintArray(heapArray, 10);

	delete[] heapArray;

	// BAD!  This memory no longer belongs to us!
	//cout << heapArray[3] << endl;
	heapArray = nullptr;

	// Safer!
	if(heapArray != nullptr)
		cout << heapArray[3] << endl;

	heapArray = new int[4];

	// Let's copy from stackArray into heapArray...
	for (int i = 0; i < 4; ++i)
	{
		heapArray[i] = stackArray[i];
	}

	cout << "Heap array after copy: " << endl;
	PrintArray(heapArray, 4);

	// Now try resizing heapArray
	int* tempArray = new int[20];
	for (int i = 0; i < 4; ++i)
	{
		tempArray[i] = heapArray[i];
	}
	delete[] heapArray;
	heapArray = tempArray;

	cout << "Heap array after resize: " << endl;
	PrintArray(heapArray, 20);


	delete[] heapArray;

	cin.get();
	return 0;
}
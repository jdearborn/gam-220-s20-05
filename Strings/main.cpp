#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
	// string myString;
	char myString[] = {'H', 'e', 'l', 'l', 'o'};

	for (int i = 0; i < 5; ++i)
	{
		cout << myString[i];
	}
	cout << endl;

	const char* myString2 = "Hello!";  // The 7th character: The Null Terminator byte

	cout << myString2 << endl;


	char myString3[6];
	for (int i = 0; i < 6; ++i)
	{
		myString3[i] = myString2[i];
	}

	cout << "Without a Null byte: " << myString3 << endl;

	string myString4 = "A string class!";

	cout << myString4 << endl;

	// C#: Read one line of text input
	// string input = Console.ReadLine();

	//cin.getline() <-- bad
	getline(cin, myString4);

	cin.get();
	return 0;
}
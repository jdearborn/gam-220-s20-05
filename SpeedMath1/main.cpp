#include <iostream>
#include <chrono>
#include <string>
using namespace std;
using namespace chrono;

void main(int argc, char* argv[])
{
	cout << "Welcome to Speed Math!" << endl;
	cout << "Your skills are about to be tested." << endl;
	cout << "Press Enter to begin..." << endl;
	cin.get();

	int numCorrect = 0;
	int numWrong = 0;

	high_resolution_clock::time_point startTime = high_resolution_clock::now();

	for (int i = 0; i < 10; ++i)
	{
		int x = rand() % 10;
		int y = rand() % 10;
		int operationIndex = rand() % 4;
		int answer = 0;

		if (operationIndex == 0) // Add
		{
			cout << x << " + " << y << " = ";
			answer = x + y;
		}
		else if (operationIndex == 1) // Subtract
		{
			cout << x << " - " << y << " = ";
			answer = x - y;
		}
		else if (operationIndex == 2) // Multiply
		{
			cout << x << " * " << y << " = ";
			answer = x * y;
		}
		else if (operationIndex == 3) // Divide
		{
			if (y == 0)
				y = 1 + rand() % 9;  // values from 1-9
			cout << x << " / " << y << " = ";
			answer = x / y;
		}

		// Wait for input
		int guess;
		string input;

		getline(cin, input);

		// Convert input into a value we can check
		guess = stoi(input);

		if (guess == answer)
		{
			numCorrect++;
		}
		else
		{
			numWrong++;
		}
	}
	high_resolution_clock::time_point stopTime = high_resolution_clock::now();

	milliseconds totalMS = duration_cast<milliseconds>(stopTime - startTime);
	int numSeconds = totalMS.count() / 1000;

	Console.WriteLine("You got " + numCorrect + " out of " + (numCorrect + numWrong));
	Console.WriteLine("Your Total Time: " + numSeconds);

	if (numWrong == 0)
	{
		Console.WriteLine("That was perfect!!!");
	}
	else if (numWrong < numCorrect)
	{
		Console.WriteLine("You did fine.");
	}
	else if (numCorrect == 0)
	{
		Console.WriteLine("Are you okay?");
	}
	else
	{
		Console.WriteLine("That was pretty bad.  Try again!");
	}

	Console.ReadLine();
}
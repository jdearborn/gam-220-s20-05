#include <iostream>
using namespace std;

#define VERSION_NUMBER 3

// Uses value semantics: `number` can change, but the input is not affected.
int Add5(int number)
{
	number = 0;
	return number + 5;
}

// Uses reference semantics: When `number` changes, the input variable changes.
void Add3(int& number)
{
	number += 3;
}

int main(int argc, char* argv[])
{
	int input = 7;
	int result = Add5(input);

	cout << "Input after Add5(): " << input << endl;
	cout << "Result of Add5(): " << result << endl;

	Add3(input);

	cout << "Input after Add3(): " << input << endl;

	Add5(3);

	cout << "Version: " << VERSION_NUMBER << endl;

#ifdef OTHER_NUMBER
	cout << "Version: " << OTHER_NUMBER << endl;
#endif

#if VERSION_NUMBER == 3
	cout << "Version is 3!" << endl;
#endif

#if VERSION_NUMBER == 4
	cout << "Version is 4!" << endl;
#endif

	//Add3(3);
	// Error1: initial value of reference to non - const must be an lvalue
	// Error2: 'void Add3(int &)': cannot convert argument 1 from 'int' to 'int &'

	// Reference to an int (an alias)
	int& george = input;
	george += 17;

	cin.get();

	return 0;
}
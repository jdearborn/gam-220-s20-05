#include <iostream>
#include <bitset>
using namespace std;

int main(int argc, char* argv[])
{
	/* Binary representation */
	//Base 2 (2 values per digit)  - Binary
	//	01000111110001
	//Base 10 (10 values)  - Decimal
	//	908762010
	
	/* Binary literals */
	int i = 0b100101;

	/* Formatting integers as binary */
	cout << 0b100101 << endl;
	cout << bitset<8>(i) << endl;

	/* Overflow */
	//0b11111111 + 1 ?
	//0b11111111 + 1 == 0b00000000
	unsigned char c = 255;
	c++;
	cout << (int)c << endl;

	/* Underflow */
	unsigned char d = 1;
	d -= 2;


	return 0;
}
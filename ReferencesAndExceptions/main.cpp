#include <iostream>
#include <stdexcept>
#include <string>
using namespace std;


void PassByValue(int value)
{
	// Changes to `value` do not persist beyond the lifetime of this function
	value = 1;
}

void PassByReference(int& refVariable)
{
	// Changes to `refVariable` do persist: They actually change the variable that was passed in.
	refVariable = 2;
}

void PassByReferenceUsingPointer(int* ptr)
{
	// If there's a problem, what to do?

	// Could do nothing:
	/*if (ptr == nullptr)
		return;*/

	// Could print a message or otherwise communicate to the user that something went wrong.
	/*if (ptr == nullptr)
	{
		cout << "Hey, that's a null pointer and I don't like it!" << endl;
		return;
	}*/

	// Could try to "fix" the problem and continue on...
	/*int hackyFix = 0;
	if (ptr == nullptr)
	{
		ptr = &hackyFix;
	}*/

	// Could throw an exception.
	if (ptr == nullptr)
	{
		//throw 1234;
		throw runtime_error("PassByReferenceUsingPointer() was given a null pointer.");
	}


	*ptr = 3;
}


int& GetValueAtIndex(int* array, int size, int index)
{
	if (index < 0)
	{
		throw out_of_range("GetValueAtIndex() was given a negative index!");
	}
	if (index >= size)
	{
		throw out_of_range("GetValueAtIndex() was given an index beyond the end of the array!");
	}
	if (array == nullptr)
	{
		throw invalid_argument("GetValueAtIndex() was given a null array!");
	}

	return array[index];
}

// Pass by value: Makes a copy of the string passed in.
void PrintString(string s)
{
	cout << s << endl;
}

// Pass by reference: No copy made...  but...
void PrintStringRef(string& s)
{
	cout << s << endl;

	// ...but what if it modifies the string?
	s += " Ha ha!  You've been modified.";
}

// Pass by constant reference: No copy made for variables, copy made for literals.  No modification allowed.
void PrintStringConstRef(const string& s)
{
	cout << s << endl;
}


int main(int argc, char* argv[])
{
	PassByValue(56);


	int myInt = 5;
	PassByReference(myInt);

	int& myIntReference = myInt;

	int* myIntPtr = new int;

	PassByReferenceUsingPointer(myIntPtr);
	PassByReferenceUsingPointer(&myInt);

	int* myNullPtr = nullptr;

	try
	{
		PassByReferenceUsingPointer(myNullPtr);
	}
	catch (int errorCode)
	{
		cout << "There was a problem.  Here's the error code: " << errorCode << endl;

		if (errorCode == 4567)
		{
			throw;
		}
	}
	catch (exception& e)
	{
		cout << "An exception was thrown: " << e.what() << endl;
	}

	delete myIntPtr;



	// Working with references

	int array[] = {1, 2, 3};
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;

	// Weird, but we can do this because this reference represents the actual integer variable in that array.
	GetValueAtIndex(array, 3, 1) = 14;
	cout << "Value at index 1: " << GetValueAtIndex(array, 3, 1) << endl;


	PrintString("Small string");
	PrintString("BIG string JSKAFN LKSAND LKSDLKAS DLKJ SALKDJASKJLK DLKS AKL DKL KL :LKS ::LS :S ALK: "
	"asfkl nalskd jlakdjaslkjdal skjd laksjd laksjdlka sjdlk jaslkdj askld a s"
	"as;ofdkjas ;lkdl sal djasjklfhas hfaosih iwknalks dmas.md "
	"aslkjsldkj asld alsknd masl kdnlksd m");

	string myString = "Some string to use as a reference...";
	PrintStringRef(myString);


	PrintStringConstRef(myString);
	PrintStringConstRef("Here's a literal string.");

	cin.get();
	return 0;
}
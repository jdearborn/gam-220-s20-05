#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	int myArray[] = { 90, 93, 82, 68, 100, 54, 101, 84, 93, 103, 77, 76, 93, 85, 70 };
	int length = 15;

	float sum = 0;
	for (int i = 0; i < length; ++i)
	{
		sum += myArray[i];
	}
	cout << "Sum of all numbers: " << sum << endl;

	float avg = sum / length;

	cout << "Average: " <<  avg << endl;

	for (int i = 0; i < length; ++i)
	{
		if (myArray[i] < 70)
		{
			cout << myArray[i] << " is less than 70" << endl;
			break;
		}
	}

	cout << "Printing numbers greater than 100" << endl;
	for (int i = 0; i < length; i++)
	{
		if (myArray[i] > 100)
		{
			cout << myArray[i] <<  " " << endl;
		}
	}
	cout << endl;

	int maxSoFar = -9999;
	int minSoFar = 9999;
	for (int i = 0; i < length; i++)
	{
		if (myArray[i] > maxSoFar)
		{
			maxSoFar = myArray[i];
		}
		else if (myArray[i] < minSoFar)
		{
			minSoFar = myArray[i];
		}
	}
	cout << "Max value: " << maxSoFar << endl;
	cout << "Min value: " << minSoFar << endl;

	cin.get();
	return 0;
}


